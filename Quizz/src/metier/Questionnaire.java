package metier;

import java.util.List;
import java.util.Vector;

public class Questionnaire {
	
	List<QR> qrs;
	
	public Questionnaire() {
		
		qrs = new Vector<QR>();
		initQuestionnaire();
	}
	
	private void initQuestionnaire() {
		
		qrs.add(new QR("Qui a cr�� le langage java?", "James Gosdling"));
		qrs.add(new QR("Qui a cr�� le langage c++?", "Bjarne Bidul"));
		qrs.add(new QR("Qui a cr�� le langage c?", "Demis Rousseau"));
	}
	
	public List<QR> getQrs(){
		return qrs;
	}

}
