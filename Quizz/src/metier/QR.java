package metier;
// un bean est un 'POJO' (un bon vieil objet JAVA)
public class QR {

	String question, reponse;
	
	public QR() {}                                //constructeur vide obligatoire pour le bean

	public QR(String question, String reponse) {
		super();
		this.question = question;
		this.reponse = reponse;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getReponse() {
		return reponse;
	}

	public void setReponse(String reponse) {
		this.reponse = reponse;
	}

	
	@Override
	public String toString() {
		return "Question [question=" + question + ", reponse=" + reponse + "]";
	}
	
	
	
	
}
