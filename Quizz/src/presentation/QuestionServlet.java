package presentation;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import metier.Questionnaire;


@WebServlet("/Questions")
public class QuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Questionnaire questionnaire;
	
	@Override
	public void init() throws ServletException{
		questionnaire = new Questionnaire();
		super.init();
	}
	
	


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//si la requete ne contient aucun param�tre (si aucun param�tre index n'y est pr�sent) elle retourne la liste compl�te
		//si elle contient le param�tre index, retourne juste la question � cette position

		String strIndex = request.getParameter("index");
		int index = 0;
		QuestionDto dto;
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		Gson gs = new Gson();
		
		if (strIndex == null) {
			String js = gs.toJson(questionnaire.getQrs());
			PrintWriter out = response.getWriter();
			out.append(js);
			out.flush();
			//�quivaut �: 
			//response.getWriter().append(gs.toJson(questionnaire)).flush();
		} else {
			index = Integer.parseInt(strIndex);
			dto = new QuestionDto();
			if ((index < 0) || (index >= questionnaire.getQrs().size())) {
				dto.setDataOK(false);
				dto.setMessage("index incorrect");
			} else {
				dto.setDataOK(true);
				dto.setQuestion(questionnaire.getQrs().get(index));
			}
			response.getWriter().append(gs.toJson(dto)).flush();
		}	
		
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
