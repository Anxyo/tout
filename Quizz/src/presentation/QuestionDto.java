package presentation;

import metier.QR;

public class QuestionDto {

	QR question;
	boolean dataOK;
	String message;
	
	public QR getQuestion() {
		return question;
	}
	public void setQuestion(QR question) {
		this.question = question;
	}
	public boolean isDataOK() {
		return dataOK;
	}
	public void setDataOK(boolean dataOK) {
		this.dataOK = dataOK;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
