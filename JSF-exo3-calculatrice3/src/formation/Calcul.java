package formation;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "calcul", eager = true)
public class Calcul {
	
	int operand1 = 0;
	int operand2 = 0;
	int resultat = 0;
	int somme = 0;
	int multiplication = 0;
	
	public int getOperand1() {
		return operand1;
	}

	public void setOperand1(int operand1) {
		this.operand1 = operand1;
	}

	public int getOperand2() {
		return operand2;
	}

	public void setOperand2(int operand2) {
		this.operand2 = operand2;
	}

	public int getResultat() {
		return resultat;
	}
	
	public int getSomme() {
		return somme;
	}
	
	public int getMultiplication() {
		return multiplication;
	}

	public String effectuer() {
		somme = new Somme(operand1, operand2).getResultat();
		multiplication = new Multiplication(operand1, operand2).getResultat();
		return "resultat";
	}
	
	

}
