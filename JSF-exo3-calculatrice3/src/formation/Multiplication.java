package formation;

public class Multiplication {

	
	int operand1 = 0;
	int operand2 = 0;
	int resultat = 0;
	
	public Multiplication(int o1, int o2) {
		operand1 = o1;
		operand2 = o2;
	}
	
	public int getOperand1() {
		return operand1;
	}

	public void setOperand1(int operand1) {
		this.operand1 = operand1;
	}

	public int getOperand2() {
		return operand2;
	}

	public void setOperand2(int operand2) {
		this.operand2 = operand2;
	}

	public int getResultat() {
		return this.operand1 * this.operand2;
	}

}
