package metier;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "JEUX")
public class JeuxDeSociete {
	
	@Id
	private Integer id;
	private String nom;
	private int nbreJoueurs, duree, edition, ageMin;
	
	public JeuxDeSociete() {}

	public JeuxDeSociete(Integer id, String nom, int nbreJoueurs, int duree, int edition, int ageMin) {
		super();
		this.id = id;
		this.nom = nom;
		this.nbreJoueurs = nbreJoueurs;
		this.duree = duree;
		this.edition = edition;
		this.ageMin = ageMin;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getNbreJoueurs() {
		return nbreJoueurs;
	}

	public void setNbreJoueurs(int nbreJoueurs) {
		this.nbreJoueurs = nbreJoueurs;
	}

	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public int getEdition() {
		return edition;
	}

	public void setEdition(int edition) {
		this.edition = edition;
	}

	public int getAgeMin() {
		return ageMin;
	}

	public void setAgeMin(int ageMin) {
		this.ageMin = ageMin;
	}
	
	
	

}
