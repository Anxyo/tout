package presentation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import metier.JeuxDeSociete;

public class principale {

	public static void main(String [] args) {
		
		//obtenir un EntityManagerFactory
		EntityManagerFactory emf = 
	            Persistence.createEntityManagerFactory("ServiceJeux");
	    EntityManager em = emf.createEntityManager();
	    
	    em.getTransaction().begin();
	    JeuxDeSociete J1 = new JeuxDeSociete(1, "Touch ma tralala", 10, 30, 1900, 7);
	    em.persist(J1);
	    JeuxDeSociete J2 = new JeuxDeSociete(2, "Elle est o� la poulette?", 5, 180, 2014, 10);
	    em.persist(J2);
	    JeuxDeSociete J3 = new JeuxDeSociete(3, "Attrape le Mickey", 6, 20, 1950, 5);
	    em.persist(J3);
	    em.getTransaction().commit();

	    
	    
	    
	    
	    em.close();
	    emf.close();
		
	}
	
}
