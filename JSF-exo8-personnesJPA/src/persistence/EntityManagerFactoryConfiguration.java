package persistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class EntityManagerFactoryConfiguration {
	
	EntityManagerFactory emf;
	
	
	
	public EntityManagerFactoryConfiguration() {
		emf = Persistence.createEntityManagerFactory("ServicePerson");
	}

	

	public EntityManagerFactory getEmf() {
		return emf;
	}



	public void setEmf(EntityManagerFactory emf) {
		this.emf = emf;
	}

}
