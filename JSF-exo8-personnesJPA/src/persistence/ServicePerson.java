package persistence;

import java.util.List;

import formation.Person;

public interface ServicePerson {

	Person creePerson(String firstName, String lastName, int age);

	List<Person> findAllPersons();

}