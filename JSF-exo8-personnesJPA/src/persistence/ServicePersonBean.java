package persistence;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;


import formation.Person;


public class ServicePersonBean implements ServicePerson {
	
	EntityManagerFactory emf;
    protected EntityManager em;
    

    
    public ServicePersonBean() {
    	EntityManagerFactoryConfiguration emfc = new EntityManagerFactoryConfiguration();
		emf = emfc.getEmf();
    	this.em = emf.createEntityManager();
	}
    
    
	@Override
	public Person creePerson(String firstName, String lastName, int age) {
    	em.getTransaction().begin();			// d�but de transaction
        //Person person = new Person(id);
        Person person = new Person(lastName, firstName, "", age);
//        person.setLastName(lastName);
//        person.setFirstName(firstName);
//        person.setAge(age);
        em.persist(person);
        em.getTransaction().commit();			// commit
        return person;
    }
	
	
	

//  public void supprimePerson(int id) {
//     TypedQuery<Person> rqt = em.createQuery("DELETE FROM personnes WHERE nom='NOM_1062' AND prenom='prenom_1062' AND age='1062'", Person.class);
//     
//
//	em.getTransaction().begin();			// d�but de transaction
//	Person person = findPerson(id);
//    if (person != null) {
//        em.remove(person);
//    }
//    em.getTransaction().commit();			// commit
//}
	
	//    public void supprimePerson(int id) {
//    	em.getTransaction().begin();			// d�but de transaction
//    	Person person = findPerson(id);
//        if (person != null) {
//            em.remove(person);
//        }
//        em.getTransaction().commit();			// commit
//    }

//    public Person augmenteSalaire(int id, long augmentation) {
//    	em.getTransaction().begin();			// d�but de transaction
//        Person person = em.find(Person.class, id);
//        if (person != null) {
//            person.setSalaire(person.getSalaire() + augmentation);
//        }
//        em.getTransaction().commit();			// commit
//        return person;
//    }

//    public Person findPerson(int id) {
//        return em.find(Person.class, id);
//    }


	@Override
	public List<Person> findAllPersons() {
        TypedQuery<Person> rqt = em.createQuery("SELECT e FROM Person e", Person.class);
        return  rqt.getResultList();
    }
	

}
