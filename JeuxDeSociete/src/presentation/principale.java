package presentation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import metier.JeuxDeSociete;

public class principale {

	public static void main(String [] args) {
		
		//obtenir un EntityManagerFactory
		EntityManagerFactory emf = 
	            Persistence.createEntityManagerFactory("ServiceJeux");
	    EntityManager em = emf.createEntityManager();
	    
	    em.getTransaction().begin();
	    JeuxDeSociete J1 = new JeuxDeSociete("Touch ma tralala", 10, 30, 1900, 7);
	    em.persist(J1);
	    JeuxDeSociete J2 = new JeuxDeSociete("Elle est o� la poulette?", 5, 180, 2014, 10);
	    em.persist(J2);
	    JeuxDeSociete J3 = new JeuxDeSociete("Attrape le Mickey", 6, 20, 1950, 5);
	    em.persist(J3);
	    em.getTransaction().commit();     //vide le cache et enregistre les donn�es dans la bdd

	    
	    String rqt = "Select J from JeuxDeSociete J";
	    TypedQuery<JeuxDeSociete> laRequete = em.createQuery(rqt, JeuxDeSociete.class);
	    List<JeuxDeSociete> listeObjets = laRequete.getResultList();
	    JeuxDeSociete js = null;
	    for (int i = 0; i < listeObjets.size(); i++) {
	    	js = (JeuxDeSociete)listeObjets.get(i);
	    	System.out.println(js);
		}
	     
	    
	    //modification d'un objet
//	    em.getTransaction().begin();
//	    JeuxDeSociete leJeu = em.find(JeuxDeSociete.class, 2);
//	    leJeu.setEdition(2019);
//	    em.getTransaction().commit();
	    
	    
	    
	    
	    //suppression
	    //on recharge l'objet
	    /**
	    em.getTransaction().begin();
	    JeuxDeSociete leJeu = em.find(JeuxDeSociete.class, 3);
	    System.out.println("nom " + leJeu.getNom());
	    em.remove(leJeu);
	    em.getTransaction().commit();
	    */
	    
	    em.close();
	    emf.close();
		
	}
	
}
