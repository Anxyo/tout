package presentation;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import metier.Client;
import service.GestionCommande;


@WebServlet("/SerletClient")
public class SerletClient extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private EntityManagerFactory emf;
	private static final String BTN_VALIDER = "btnValider";
	

	@Override
	public void init() throws ServletException{
		emf = (EntityManagerFactory)getServletContext().getAttribute("emf");
		super.init();
	}
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//cr�er un EM
		EntityManager em = emf.createEntityManager();
		//instancier la couche de service et lui passer l'EM
		GestionCommande gc = new GestionCommande(em);
		
		if (BTN_VALIDER != null) {
			
			String nom = request.getParameter("nomClient");
			String prenom = request.getParameter("prenomClient");
			String adresse =  request.getParameter("adresseClient");
			String email = request.getParameter("emailClient");
			System.out.println(nom + prenom + adresse + email);
			Client clt = gc.creeClient(nom, prenom, adresse, email);
			System.out.println(clt);
			
		} else {	
		}
		
		em.close();
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
