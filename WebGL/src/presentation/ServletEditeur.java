package presentation;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.GestionEdition;


@WebServlet("/ServletEditeur")
public class ServletEditeur extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private EntityManagerFactory emf;

       
	@Override
	public void init(ServletConfig config) throws ServletException{
		emf = (EntityManagerFactory) getServletContext().getAttribute("emf");
		super.init(config);
	}
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//cr�er un EM
		EntityManager em = emf.createEntityManager();
		//instancier la couche de service et lui passer l'EM
		GestionEdition ge = new GestionEdition(em);
			
				
				
				
				
		em.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
