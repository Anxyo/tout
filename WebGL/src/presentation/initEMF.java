package presentation;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class initEMF implements ServletContextListener {

	private EntityManagerFactory emf = null;
	
    public void contextInitialized(ServletContextEvent sce)  { 
    	emf = Persistence.createEntityManagerFactory("EditeurLivres");
    	sce.getServletContext().setAttribute("emf", emf);
    }
    
    public void contextDestroyed(ServletContextEvent sce)  { 
    	emf.close();
    }
}
