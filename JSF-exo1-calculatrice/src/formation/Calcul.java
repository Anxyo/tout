package formation;

import javax.faces.bean.ManagedBean;

@ManagedBean(name = "calcul", eager = true)
public class Calcul {
	
	int o1 = 0;
	int o2 = 0;
	int resultat = 0;
	
	
	public int getO1() {
		return o1;
	}
	
	public void setO1(int o1) {
		this.o1 = o1;
	}
	
	public int getO2() {
		return o2;
	}
	
	public void setO2(int o2) {
		this.o2 = o2;
	}
	
	public int getResultat() {
		return resultat;
	}
	
	public void effectuer() {
		resultat = o1 + o2;
	}
	

}
