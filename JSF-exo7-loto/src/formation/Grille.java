package formation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "grille", eager = true)
@SessionScoped
public class Grille {

	List<List<Integer>> lignes = new ArrayList<List<Integer>>();
	List<Integer> playerSelect = new ArrayList<Integer>();

	String couleur = "lightgray";
	Random r = new Random();
	
	public Grille() {
		for (int l = 0; l < 10; l++) {
			List<Integer> ligne = new ArrayList<Integer>();
			for (int c = 0; c < 5; c++) {
				ligne.add(l + c * 10);
			}
			lignes.add(ligne);
		}
	}

	public List<List<Integer>> getLignes() {
		return lignes;
	}

	public void selection(int unecase) {
		if (!playerSelect.contains(unecase)) {
			
			if (playerSelect.size() < 6) {
				playerSelect.add(unecase);
				System.out.println(unecase);
				System.out.println(playerSelect);
			}
		}else {
			playerSelect.remove(new Integer(unecase));
			System.out.println(playerSelect);
		}

	}

	public String getCouleur(int unecase) {
		return playerSelect.contains(unecase) ? "green" : "lightgray";
	}
	
	public void flash() {
		
		List<Integer> tirageAuto = new ArrayList<Integer>();
		int nbAlea = r.nextInt(49);
		
	}
	

}
