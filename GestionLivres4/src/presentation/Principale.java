package presentation;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;

import service.GestionCommande;
import service.ICommandes;

public class Principale {

	public static void main(String[] args) {
		// objet complexe, lourd et threadsafe
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("EditeurLivres");
		// objet et non thread-safe
		EntityManager em1 = emf.createEntityManager();
		//EntityManager em2 = emf.createEntityManager();
		
//		IEdition ie = new GestionEdition(em2);
//		Editeur ed = ie.creeEditeur("Gallimard", "12 rue de la Lune");
//		ie.publieLivre(ed, "L'�tranger", "0321127420", "Albert Camus",
//				new Date(), 20.50, new String[] {"Chapitre 1", "Chapitre 2", "Chapitre 3" }, 
//									new int[] {	34, 42, 55 });
//		ie.publieLivre(ed, "La peste", "0321127450", "Albert Camus", new Date(), 
//				22.50, new String[] {"Chapitre 1", "Chapitre 2", "Chapitre 3" }, 
//					   new int[] { 34, 53, 75 });
//		List<Editeur> liste1 = ie.listeEditeurs();
//		for (Editeur unEditeur : liste1) {
//			out.println(unEditeur);
//		}
//
//		out.println("=============================================================================");
		// gestion des clients
		ICommandes ic = new GestionCommande(em1);
//		Client clt = ic.creeClient("Lamy", "Marc", "12 rue des amis",
//				"blamy@gmail.com");
//		Client clt2 = ic.creeClient("Lamyx", "Marcx", "12 rue des amix",
//				"blamyx@gmail.com");
//		ic.creeCommande(clt.getId(), new Date(System.currentTimeMillis()), 34.50);
//		ic.creeCommande(clt.getId(), new Date(System.currentTimeMillis()), 43.0);
//		ic.creeCommande(clt2.getId(), new Date(System.currentTimeMillis()), 76.0);
//
//		List<Client> liste2 = ic.listeClients();
//		for (Client unClient : liste2) {
//			out.println(unClient);
//		}
		
		Endpoint ep = Endpoint.publish("http://localhost:8888/clients", ic);
		
		try {
			Thread.sleep(1000*60*20);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		em1.close();
		//em2.close();
		emf.close();
	}
}
