package service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import metier.Client;
import metier.Commande;

@WebService
public interface ICommandes {
	// cr�er un Client
	Client creeClient(String nom, String prenom, String adresse, String email);
	// ajouter une commande
	Commande creeCommande(long idClient, Date dateCommande, Double montant);
	// lister les commandes d'un client
	List<Commande> listeCommandes(long idClient);
	// lister les clients
	List<Client> listeClients();
}
