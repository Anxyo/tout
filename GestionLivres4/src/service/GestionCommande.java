package service;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import metier.Client;
import metier.Commande;

@WebService(endpointInterface = "service.ICommandes")
public class GestionCommande implements ICommandes {
	//List<Client> listeClients = new ArrayList<Client>();
	EntityManager em;
	
	public GestionCommande(EntityManager em1) {
		this.em = em1;
	}

	@Override
	public Client creeClient(String nom, String prenom, String adresse,
			String email) {
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		Client clt = new Client();
		clt.setNom(nom);
		clt.setPrenom(prenom);
		clt.setAdresse(adresse);
		clt.setEmail(email);
		em.persist(clt);
		tx.commit();
		return clt;
	}

	@Override
	public Commande creeCommande(long idClient, Date dateCommande, Double montant) {
		EntityTransaction tx = em.getTransaction();
		tx.begin();
		// recharge le client persist�
		Client leClient = em.find(Client.class, idClient);
		Commande cmd = new Commande();
		cmd.setDateCommande(dateCommande);
		cmd.setMontant(montant);
		cmd.setClient(leClient);
		leClient.getCommandes().add(cmd); // modifie le client
		tx.commit();
		return cmd;
	}

	@Override
	public List<Commande> listeCommandes(long idClient) {
		// "Select c from Commande c Where c.client.id =" + clt.getId()
		Client clt = em.find(Client.class, idClient);
		return clt.getCommandes();
	}

	@Override
	public List<Client> listeClients() {
		TypedQuery<Client> q1 = em.createNamedQuery("Client.FindAll", Client.class);
		List<Client> l = q1.getResultList();
		return l;
	}

}
