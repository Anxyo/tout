package commun;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAHelper {
	private static EntityManagerFactory emf;
	
	static {
		emf = Persistence.createEntityManagerFactory("EditeurLivres");
	}
	
	public static EntityManager getEM(){
		return emf.createEntityManager();
	}
	
	public static void closeEMF(){
		emf.close();
	}
	
}
