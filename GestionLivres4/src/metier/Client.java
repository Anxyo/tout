package metier;

import java.util.ArrayList;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQuery(name="Client.FindAll", query="select c from Client c")

@Entity
public class Client {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String nom, prenom, adresse,email;
	
	@OneToMany(mappedBy="client", 
			cascade=CascadeType.ALL, 
			fetch=FetchType.EAGER)
	private List<Commande> commandes;
	
	public Client(){
		commandes = new ArrayList<Commande>();
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Client id " + id + ", " + nom + " " + prenom + ", " + adresse + "\n");
		for (Commande c : commandes){
			sb.append(c);
		}
		return sb.toString();
	}
	
}
