package metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Livre {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String noISBN;
	private String titre, auteur;
	private Date datePublication;
	private Double prix;
	
	@ManyToOne
	private Editeur editeur;
	
	@OneToMany(mappedBy="livre", 
			   cascade=CascadeType.ALL, 
			   fetch=FetchType.EAGER)
	private List<Chapitre> chapitres;
	
	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}
	
	public List<Chapitre> getChapitres() {
		return chapitres;
	}

	public void setChapitres(List<Chapitre> chapitres) {
		this.chapitres = chapitres;
	}

	public Livre(){
		chapitres = new ArrayList<Chapitre>();
	}
	
	public String getNoISBN() {
		return noISBN;
	}
	public void setNoISBN(String noISBN) {
		this.noISBN = noISBN;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public Date getDatePublication() {
		return datePublication;
	}
	public void setDatePublication(Date datePublication) {
		this.datePublication = datePublication;
	}
	public Double getPrix() {
		return prix;
	}
	public void setPrix(Double prix) {
		this.prix = prix;
	}
	public Editeur getEditeur() {
		return editeur;
	}
	public void setEditeur(Editeur editeur) {
		this.editeur = editeur;
	}
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Livre id " + id + ", " + titre + " de " + auteur + "\n");
		for(Chapitre ch:chapitres){
			sb.append(ch + "\n");
		}
		return sb.toString();
	}


}
