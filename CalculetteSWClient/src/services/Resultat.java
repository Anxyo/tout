
package services;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour resultat complex type.
 * 
 * <p>Le fragment de sch�ma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="resultat">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operande1" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="operande2" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="operateur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resultat" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "resultat", propOrder = {
    "operande1",
    "operande2",
    "operateur",
    "resultat"
})
public class Resultat {

    protected double operande1;
    protected double operande2;
    protected String operateur;
    protected double resultat;

    /**
     * Obtient la valeur de la propri�t� operande1.
     * 
     */
    public double getOperande1() {
        return operande1;
    }

    /**
     * D�finit la valeur de la propri�t� operande1.
     * 
     */
    public void setOperande1(double value) {
        this.operande1 = value;
    }

    /**
     * Obtient la valeur de la propri�t� operande2.
     * 
     */
    public double getOperande2() {
        return operande2;
    }

    /**
     * D�finit la valeur de la propri�t� operande2.
     * 
     */
    public void setOperande2(double value) {
        this.operande2 = value;
    }

    /**
     * Obtient la valeur de la propri�t� operateur.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperateur() {
        return operateur;
    }

    /**
     * D�finit la valeur de la propri�t� operateur.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperateur(String value) {
        this.operateur = value;
    }

    /**
     * Obtient la valeur de la propri�t� resultat.
     * 
     */
    public double getResultat() {
        return resultat;
    }

    /**
     * D�finit la valeur de la propri�t� resultat.
     * 
     */
    public void setResultat(double value) {
        this.resultat = value;
    }

}
