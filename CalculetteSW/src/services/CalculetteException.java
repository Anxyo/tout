package services;

public class CalculetteException extends Exception {

	private static final long serialVersionUID = 1L;

	public CalculetteException() {
	}

	public CalculetteException(String message) {
		super(message);
	}

	public CalculetteException(String message, Throwable cause) {
		super(message, cause);
	}


}
