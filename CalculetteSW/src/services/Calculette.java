package services;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class Calculette {
	
	public Resultat addition(@WebParam(name="a") double a, @WebParam(name="b") double b) {
		System.out.println("addition appel�e");
		return new Resultat(a, b, a+b, "+");
	}
	
	public Resultat soustraction(double a, double b) {
		return new Resultat(a, b, a-b, "-");
	}
	
	public Resultat multiplication(double a, double b) {
		return new Resultat(a, b, a*b, "*");
	}
	
	public Resultat division(double a, double b) throws CalculetteException {
		if (b == 0) throw new CalculetteException("la t�te � Toto");
		return new Resultat(a, b, a/b, "/");
	}

}
