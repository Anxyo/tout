package services;

import java.io.Serializable;

public class Resultat implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private double operande1, operande2, resultat;
	private String operateur;
	
	
	public Resultat(){}
	
	public Resultat(double operande1, double operande2, double resultat, String operateur) {
		super();
		this.operande1 = operande1;
		this.operande2 = operande2;
		this.resultat = resultat;
		this.operateur = operateur;
	}
	
	
	public double getOperande1() {
		return operande1;
	}
	
	public void setOperande1(double operande1) {
		this.operande1 = operande1;
	}
	
	public double getOperande2() {
		return operande2;
	}
	
	public void setOperande2(double operande2) {
		this.operande2 = operande2;
	}
	
	public double getResultat() {
		return resultat;
	}
	
	public void setResultat(double resultat) {
		this.resultat = resultat;
	}
	
	public String getOperateur() {
		return operateur;
	}
	
	public void setOperateur(String operateur) {
		this.operateur = operateur;
	}

	
	@Override
	public String toString() {
		return "Resultat [operande1=" + operande1 + ", operande2=" + operande2 + ", resultat=" + resultat
				+ ", operateur=" + operateur + "]";
	}

}
