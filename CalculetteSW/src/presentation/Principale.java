package presentation;

import javax.xml.ws.Endpoint;

import services.Calculette;

public class Principale {

	public static void main(String[] args) {
		
		Calculette calc = new Calculette();
		Endpoint ep = Endpoint.publish("http://localhost:8888/calculette", calc);
		System.out.println("Publication OK: " + ep.isPublished());
	}

}
