package formation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "calcul", eager = true)
@SessionScoped
public class Calcul {
	
	int nbAlea = (int) Math.round(Math.random() * 100);
	int valuePlayer;
	
	
	public int getValuePlayer() {
		return valuePlayer;
	}
	
	public void setValuePlayer(int valuePlayer) {
		this.valuePlayer = valuePlayer;
	}
	
	public int getNbalea() {
		return nbAlea;
	}
	
	public String effectuer() {

		return "resultat";
	}

}
