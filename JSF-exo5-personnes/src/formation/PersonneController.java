package formation;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "personne", eager = true)
@SessionScoped
public class PersonneController {

	String nom, prenom;
	int age;
	List<Personne> personnes = new ArrayList<Personne>();
	

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public void creerPersonne() {
		
		 personnes.add(new Personne(nom, prenom, age));
	}

	public List<Personne> getPersonnes() {
		return personnes;
	}
	
	
	
}
