package formation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class ServiceFormationDAO implements serviceDAO <Formation>{
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("dbformations3");
	@Override
	public void ajouter(Formation entite) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            
        	transaction = manager.getTransaction();
            transaction.begin();
            Formation t = new Formation(entite.getId(), entite.getLibelle());
            manager.merge(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
	}

	@Override
	public void supprimer(int id) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
        	
            transaction = manager.getTransaction();
            transaction.begin();
            Formation t = manager.find(Formation.class, id);
            manager.remove(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
 	}

	@Override
	public List<Formation> lister() {
        List<Formation> Formations = null;

        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = manager.getTransaction();
            transaction.begin();
            TypedQuery<Formation> query = manager.createNamedQuery("Formation.findAll", Formation.class); 	
            Formations = query.getResultList();	
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
        return Formations;
	}

	@Override
	public Formation trouver(int id) {
			Formation Formation = null;

	        // Create an EntityManager
	        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	        EntityTransaction transaction = null;

	        try {
	            transaction = manager.getTransaction();
	            transaction.begin();
	            TypedQuery<Formation> query = manager.createNamedQuery("Formation.findAll", Formation.class); 	
	            //totos = query.getResultList();	
	            transaction.commit();
	        } catch (Exception ex) {
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
	        return Formation;
	}
}

