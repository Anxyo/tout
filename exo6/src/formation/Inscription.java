package formation;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the inscriptions database table.
 * 
 */
@Entity
@Table(name="inscriptions")
@NamedQuery(name="Inscription.findAll", query="SELECT i FROM Inscription i")
public class Inscription implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.DATE)
	private Date debut;

	private int duree;

	//bi-directional many-to-one association to Eleve
	@ManyToOne
	@JoinColumn(name="ideleve")
	private Eleve eleve;

	//bi-directional many-to-one association to Formation
	@ManyToOne
	@JoinColumn(name="idformation")
	private Formation formation;

	public Inscription() {
	}

	public Inscription(int id, Date debut, int duree, Eleve eleve, Formation formation) {
		this.id = id;
		this.debut = debut;
		this.duree = duree;
		this.eleve = eleve;
		this.formation = formation;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDebut() {
		return this.debut;
	}

	public void setDebut(Date debut) {
		this.debut = debut;
	}

	public int getDuree() {
		return this.duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public Eleve getEleve() {
		return this.eleve;
	}

	public void setEleve(Eleve eleve) {
		this.eleve = eleve;
	}

	public Formation getFormation() {
		return this.formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

}