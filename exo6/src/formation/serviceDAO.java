package formation;

import java.util.List;

public interface serviceDAO<T> {

	void ajouter (T entite);
	void supprimer (int id);
	List<T> lister ();
	T trouver (int id);
	
}
