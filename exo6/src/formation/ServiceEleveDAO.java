package formation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class ServiceEleveDAO implements serviceDAO <Eleve>{
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("dbformations3");
	@Override
	public void ajouter(Eleve entite) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            
        	transaction = manager.getTransaction();
            transaction.begin();
            Eleve t = new Eleve(entite.getId(), entite.getNom());
            manager.merge(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
	}

	@Override
	public void supprimer(int id) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
        	
            transaction = manager.getTransaction();
            transaction.begin();
            Eleve t = manager.find(Eleve.class, id);
            manager.remove(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
 	}

	@Override
	public List<Eleve> lister() {
        List<Eleve> eleves = null;

        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = manager.getTransaction();
            transaction.begin();
            TypedQuery<Eleve> query = manager.createNamedQuery("Eleve.findAll", Eleve.class); 	
            eleves = query.getResultList();	
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
        return eleves;
	}

	@Override
	public Eleve trouver(int id) {
			Eleve eleve = null;

	        // Create an EntityManager
	        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	        EntityTransaction transaction = null;

	        try {
	            transaction = manager.getTransaction();
	            transaction.begin();
	            TypedQuery<Eleve> query = manager.createNamedQuery("Eleve.findAll", Eleve.class); 	
	            //totos = query.getResultList();	
	            transaction.commit();
	        } catch (Exception ex) {
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
	        return eleve;
	}
}

