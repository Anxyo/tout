package formation;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the formations database table.
 * 
 */
@Entity
@Table(name="formations")
@NamedQuery(name="Formation.findAll", query="SELECT f FROM Formation f")
public class Formation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	private String libelle;

	//bi-directional many-to-one association to Inscription
	@OneToMany(mappedBy="formation")
	private List<Inscription> inscriptions;

	public Formation() {
	}

	public Formation(int id, String libelle) {
		this.id=id;
		this.libelle = libelle;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Inscription> getInscriptions() {
		return this.inscriptions;
	}

	public void setInscriptions(List<Inscription> inscriptions) {
		this.inscriptions = inscriptions;
	}

	public Inscription addInscription(Inscription inscription) {
		getInscriptions().add(inscription);
		inscription.setFormation(this);

		return inscription;
	}

	public Inscription removeInscription(Inscription inscription) {
		getInscriptions().remove(inscription);
		inscription.setFormation(null);

		return inscription;
	}

}