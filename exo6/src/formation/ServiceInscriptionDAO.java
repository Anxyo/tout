package formation;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class ServiceInscriptionDAO implements serviceDAO <Inscription>{
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("dbformations3");
	@Override
	public void ajouter(Inscription entite) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            
        	transaction = manager.getTransaction();
            transaction.begin();
            Inscription t = new Inscription(entite.getId(), entite.getDebut(), entite.getDuree(), entite.getEleve(), entite.getFormation());
            manager.persist(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
	}

	@Override
	public void supprimer(int id) {
        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
        	
            transaction = manager.getTransaction();
            transaction.begin();
            Inscription t = manager.find(Inscription.class, id);
            manager.remove(t);
            transaction.commit();
            
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
 	}

	@Override
	public List<Inscription> lister() {
        List<Inscription> Inscriptions = null;

        // Create an EntityManager
        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
        EntityTransaction transaction = null;

        try {
            transaction = manager.getTransaction();
            transaction.begin();
            TypedQuery<Inscription> query = manager.createNamedQuery("Inscription.findAll", Inscription.class); 	
            Inscriptions = query.getResultList();	
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        } finally {
            manager.close();
        }
        return Inscriptions;
	}

	@Override
	public Inscription trouver(int id) {
			Inscription Inscription = null;

	        // Create an EntityManager
	        EntityManager manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	        EntityTransaction transaction = null;

	        try {
	            transaction = manager.getTransaction();
	            transaction.begin();
	            TypedQuery<Inscription> query = manager.createNamedQuery("Inscription.findAll", Inscription.class); 	
	            //totos = query.getResultList();	
	            transaction.commit();
	        } catch (Exception ex) {
	            if (transaction != null) {
	                transaction.rollback();
	            }
	            ex.printStackTrace();
	        } finally {
	            manager.close();
	        }
	        return Inscription;
	}
}

