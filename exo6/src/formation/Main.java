package formation;

import java.sql.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class Main {
    // Create an EntityManagerFactory when you start the application.
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence.createEntityManagerFactory("dbformations3");
    
    public static void main(String[] args) {
    
    	//****************************************
        // Gestion des eleves
    	//****************************************
    	
    	ServiceEleveDAO sedao = new ServiceEleveDAO();
    	//sedao.ajouter(new Eleve(1,"TOTO1"));
    	//sedao.ajouter(new Eleve(2,"TOTO2"));
    	
	    //Lire les eleves
    	List<Eleve> le = sedao.lister();
    	for (Eleve e : le)
    	{
    		System.out.println(e.getNom());
    	}
    	// Supprimer
    	sedao.supprimer(9);
    	
    	
    	
    	//****************************************
        // Gestion des formations
    	//****************************************
    	/*
    	ServiceFormationDAO sfdao = new ServiceFormationDAO();
    	sfdao.ajouter(new Formation(1,"Formation 1"));
    	sfdao.ajouter(new Formation(2,"Formation 2"));
    	
	    //Lire les Formations
    	List<Formation> lf = sfdao.lister();
    	for (Formation f : lf)
    	{
    		System.out.println(f.getLibelle());
    	}
    	// Supprimer
    	//sfdao.supprimer(5);
    	*/
    	
    	//****************************************
        // Gestion des inscriptions
    	//****************************************
    	/*
    	ServiceInscriptionDAO sidao = new ServiceInscriptionDAO();
    	Date d = new Date(123456788);
    	sidao.ajouter(new Inscription(0, d, 3, new Eleve (6, "TOTO3"), new Formation (1,"Formation 1")));
    	
	    //Lire les Formations
    	List<Inscription> li = sidao.lister();
    	for (Inscription i : li)
    	{
    		System.out.println(i.getFormation().getLibelle() + "::" + i.getEleve().getNom());
    	}
    	*/
    	// Supprimer
    	//sfdao.supprimer(5);
    	
    }
 }